﻿''' <summary>
''' The report element.
''' The report element consists of a key value pair.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="06/16/2011" by="David" revision="1.0.4184.x">
''' Created
''' </history>
Public Class ReportElement

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ReportElement" /> class.
    ''' </summary>
    ''' <param name="record">The record.</param>
    ''' <param name="valueColumnPosition">The value column position.</param>
    Public Sub New(ByVal record As String, ByVal valueColumnPosition As Short)
        MyBase.new()
        Me.Parse(record, valueColumnPosition)
    End Sub

#End Region

#Region " MEMBERS "

    ''' <summary>
    ''' Gets or sets the key.
    ''' </summary>
    Public Property Key As String

    ''' <summary>
    ''' Gets or sets the value.
    ''' </summary>
    Public Property Value As String

#End Region

#Region " PARSER "

    ''' <summary>
    ''' Gets or sets the error sentinel
    ''' </summary>
    Public Property ErrorOccurred As Boolean

    ''' <summary>
    ''' Gets or sets the error message.
    ''' </summary>
    Public Property ErrorMessage As String

    ''' <summary>
    ''' Parses the specified record.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="valueColumnPosition">The value column position.</param>
    ''' <remarks>Parses a record of the format: key: value, where the value begins at the specified column.</remarks>
    Public Function Parse(ByVal value As String, ByVal valueColumnPosition As Short) As Boolean
        ErrorMessage = ""
        ErrorOccurred = False
        If String.IsNullOrWhiteSpace(value.Trim) Then
            ErrorMessage = "Empty record"
            ErrorOccurred = True
            Return False
        End If
        If valueColumnPosition <= 1 OrElse valueColumnPosition >= value.Length Then
            ErrorMessage = "Invalid value column position"
            ErrorOccurred = True
        End If
        Me.Key = value.Substring(0, value.IndexOf(":"c))
        Me.Value = value.Substring(valueColumnPosition - 1)
        Return ErrorOccurred
    End Function

#End Region


End Class
