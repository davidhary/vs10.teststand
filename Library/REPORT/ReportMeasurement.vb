﻿''' <summary>
''' Parses Test Stand report measurement.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="06/16/2011" by="David" revision="1.0.4184.x">
''' Created
''' </history>
Public Class ReportMeasurement

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ReportParser" /> class.
    ''' </summary>
    Public Sub New(ByVal location As Integer, ByVal records As List(Of String))
        MyBase.new()
        Me._LastLocation = location
        Me._records = records
        Me.ErrorMessage = ""
    End Sub

#End Region

#Region " DATA RETRIEVAL "

    ''' <summary>
    ''' Returns the specified Measurement element key.
    ''' </summary>
    ''' <param name="value">The element.</param>
    Public Function MeasurementElementKey(ByVal value As MeasurementDataElement) As String
        Dim key As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(key)
        Dim attributes As ComponentModel.DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(ComponentModel.DescriptionAttribute), False), ComponentModel.DescriptionAttribute())
        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            key = attributes(0).Description
        End If
        Return key
    End Function

    Private _MeasurementRecords As Dictionary(Of String, String)
    ''' <summary>
    ''' Returns the specified Measurement element value.
    ''' </summary>
    ''' <param name="value">The element.</param>
    Public Function MeasurementElementValue(ByVal value As MeasurementDataElement) As String
        Dim v As String = ""
        If Me._MeasurementRecords IsNot Nothing AndAlso Me._MeasurementRecords.Count > 0 Then
            Me._MeasurementRecords.TryGetValue(Me.MeasurementElementKey(value), v)
        End If
        Return v
    End Function

    ''' <summary>
    ''' Determines whether this instance has records.
    ''' </summary>
    ''' <returns><c>True</c> if this instance has records; otherwise, <c>False</c>.</returns>
    Public Function HasRecords() As Boolean
        Return Me._MeasurementRecords IsNot Nothing AndAlso Me._MeasurementRecords.Count > 0
    End Function

    ''' <summary>
    ''' Gets or sets the module name.
    ''' </summary>
    Public Property Name As String

#End Region

#Region " PARSE"

    ''' <summary>
    ''' Gets or sets the error sentinel
    ''' </summary>
    Public Property ErrorOccurred As Boolean

    ''' <summary>
    ''' Gets or sets the error message.
    ''' </summary>
    Public Property ErrorMessage As String

    ''' <summary>
    ''' The last location parsed.
    ''' </summary>
    Public Property LastLocation As Integer

    Private _records As List(Of String)
    ''' <summary>
    ''' Gets the record.
    ''' </summary>
    Public ReadOnly Property Record As String
        Get
            Return Me._records(Me._LastLocation)
        End Get
    End Property

    ''' <summary>
    ''' Gets the next record.
    ''' </summary>
    Public ReadOnly Property NextRecord As String
        Get
            Return Me._records(Me._LastLocation + 1)
        End Get
    End Property

    ''' <summary>
    ''' Determines whether [is end measurement].
    ''' </summary>
    ''' <returns><c>True</c> if [is end measurement]; otherwise, <c>False</c>.</returns>
    Public Function IsEndMeasurement() As Boolean
        Return Me.Record.Trim.StartsWith("Status:", StringComparison.OrdinalIgnoreCase) OrElse Me.Record.Trim.StartsWith("Module Time:", StringComparison.OrdinalIgnoreCase)
    End Function

    ''' <summary>
    ''' Lists the measurements.
    ''' </summary>
    Public Function ListMeasurements() As String
        Dim builder As New Text.StringBuilder
        If Me.HasRecords Then
            For Each r As KeyValuePair(Of String, String) In Me._MeasurementRecords
                If builder.Length > 0 Then
                    builder.AppendLine()
                End If
                builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "      {0}: {1}", r.Key, r.Value)
            Next
        End If
        Return builder.ToString
    End Function

    ''' <summary>
    ''' Parses this instance.
    ''' </summary>
    Public Function Parse() As Boolean
        Me.ErrorMessage = ""
        Me.ErrorOccurred = False
        Me._MeasurementRecords = New Dictionary(Of String, String)
        Dim keyValue As KeyValuePair(Of String, String) = ReportModule.ParseRecord(Me.Record)
        Me.Name = keyValue.Key
        If Not String.IsNullOrWhiteSpace(keyValue.Value) Then
            Me._MeasurementRecords.Add("Status", keyValue.Value)
        End If
        Do
            Me.LastLocation += 1
            Dim r As String = Me.Record
            If Not String.IsNullOrWhiteSpace(r) Then
                keyValue = ReportModule.ParseRecord(r)
                If Not String.IsNullOrWhiteSpace(keyValue.Value) Then
                    If keyValue.Key.Equals("Measurement") Then
                        Me._MeasurementRecords.Add("Data", keyValue.Value)
                    Else
                        Me._MeasurementRecords.Add(keyValue.Key, keyValue.Value)
                    End If
                End If
            End If
        Loop Until (Me.IsEndMeasurement)
        Return Me.HasRecords
    End Function

#End Region

End Class

''' <summary>
''' Enumerates thw measurement data elements
''' </summary>
Public Enum MeasurementDataElement
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Measurement")> Measurement
    <ComponentModel.Description("Units")> Units
    <ComponentModel.Description("Low")> LowLimit
    <ComponentModel.Description("High")> HighLimit
    <ComponentModel.Description("Comparison Type")> ComparisonType
    <ComponentModel.Description("Module Time")> ModuleTime
    <ComponentModel.Description("Data")> Data
    <ComponentModel.Description("Status")> Status

End Enum