Imports System
Imports TSSyncLib
Imports NationalInstruments.TestStand.Interop.API

Public Class UseTSQueue

    Private Sub DequeueButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DequeueButton.Click
        Dim myEngine As Engine
        Dim mySyncManager As ISyncManager
        Dim myQueue As Queue
        Dim waitRes As WaitResult
        Dim alreadyExists As Boolean

        'Create Reference to the TestStand Engine
        myEngine = New Engine()

        'Get a reference to the TestStand Sync Manager
        mySyncManager = myEngine.GetSyncManager("*myqueue")
        'Get a reference to the Queue created in TestStand
        myQueue = mySyncManager.CreateQueue("*myqueue", -1, alreadyExists)
        MessageBox.Show(myQueue.Name)
        'As long as the queue is already created dequeue
        If alreadyExists Then
            Dim queueObject As PropertyObject
            'Must create new property object to store the value in
            queueObject = myEngine.NewPropertyObject(PropertyValueTypes.PropValType_String, False, "", 0)
            myQueue.Dequeue(True, True, queueObject, 0, Nothing, False, waitRes)

            If (waitRes = WaitResult.WaitResult_Success) Then
                QueueElement.Text = queueObject.GetValString("", 0)
            End If

        End If
    End Sub
End Class
